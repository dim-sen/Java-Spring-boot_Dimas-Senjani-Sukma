<h1>(2) Version Control and Branch Management (Git)</h1>
<h2>Resume</h2>
<p>Dalam materi ini, mempelajari:</p>

<ol>
<li>Install Git Windows</li>
<li>Staging Area</li>
<li>File .gitignore</li>
</ol>

<h3>Install Git Windows</h3>
<p>
1. Download the latest Git for windows installer
2. Install file Git setup (.exe) then follow the next and finish
3. Open a Command Primpt or Git Bash.
</p>

<h3>Staging Area</h3>
<p>
Staging area adalah sebuah file yang berada pada Git directory yang akan menyimpan informasi mengenai data mana saja yang akan masuk kedalam commit berikutnya.
</p>

<h3>File .gitignore</h3>
<p>
.gitignore merupakan sebuah file yang berisi daftar nama-nama file dan direktori yang akan diabaikan oleh Git.
</p>

<h2>Task</h2>
<h3>1. Buat sebuah repository di Github</h3>
<h3>2. Implementasi penggunaan braching yang terdiri dari master, development, featureA, dan featureB</h3>
<h3>3. Implementasi instruksi git: push, pull, stash, dan merge</h3>
<h3>4. Implementasi penggunaan conflict di branch development</h3>
<h3>5. Gunakan merge no fast forward</h3>

<p>output</p>

<p>
<a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/dim-sen/Java-Spring-boot_Dimas-Senjani-Sukma/-/blob/main/2_Version%20Control%20and%20Branch%20Management%20(Git)/screenshots/github_branching.png"/>
</p>
